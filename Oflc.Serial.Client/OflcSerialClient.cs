﻿using System;
using System.IO.Ports;

// Heavily based on the Python Flask API: https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi/src/master/oflc.py
namespace Oflc.Serial.Client
{
    public class OflcSerialClient
    {        
        private readonly SerialPort _serialPort;

        public OflcSerialClient()
        {
            _serialPort = new SerialPort();
        }

        public string[] GetAvailableSerialPorts()
        {
            return SerialPort.GetPortNames();
        }

        public string GetResponse()
        {
            return _serialPort.ReadLine();
        }

        public void SetPower(string power)
        {
            _serialPort.Write("setpower, " + power + "\n");
        }

        public void SetOutput(string output)
        {
            _serialPort.Write("setoutput, " + output + "\n");
        }

        public void SetMode(string mode)
        {
            _serialPort.Write("setmode, " + mode + "\n");
        }

        public void SetTimer(string timer)
        {
            _serialPort.Write("settimer, " + timer + "\n");
        }

        public void SetDebug(string debug)
        {
            _serialPort.Write("setdebug, " + debug + "\n");
        }

        public void SetBrightness(string brightness)
        {
            _serialPort.Write("setbrightness, " + brightness + "\n");
        }

        public void SetCustom(string custom)
        {
            _serialPort.Write("setcustom, " + custom + "\n");
        }

        public void Open(string portName)
        {
            _serialPort.PortName = portName;
            _serialPort.Open();
        }

        public void Close()
        {
            _serialPort.Close();
        }
    }
}
